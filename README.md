# My project's README

## 本地环境
* node版本必须大于或等于v6.5.0
* npm install / cnpm install 安装包依赖包
* npm start 启动服务

## 线上环境
* npm build


## 技术选型
* react
* antd-design
* react-redux
* react-router
