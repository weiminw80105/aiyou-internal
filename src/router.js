import React from "react";
import { Router } from "dva/router";
// import IndexPage from './routes/IndexPage';

// import Users from './routes/Users.js';
// import Wallet from './routes/Wallet.js';
// import AmazonBankManagement from './routes/AmazonBankManagement';
// import WithdrawWizard from './routes/Withdraw';
// import RealNameAuthentication from "./routes/RealNameAuthentication.js";
// import WithdrawBankManagement from './routes/WithdrawBankManagement';
// import NewWithdrawBankAccount from './routes/NewWithdrawBankAccount';
// import MiddlewareTips from './routes/MiddlewareTips';

// function RouterConfig({ history }) {
//   return (
//     <Router history={history}>
//       <Route path="/" component={IndexPage} />
//       <Route path="/users" component={Users} />
//       <Route path="/home" component={Wallet} />
//       <Route path="/amazon-bank-accounts" component={AmazonBankManagement} />
//       <Route path="/withdraw" component={WithdrawWizard} />
//       <Route path="/withdraw-bank-accounts" component={WithdrawBankManagement} />
//       <Route path="/withdraw-bank-accounts/new" component={NewWithdrawBankAccount} />
//       <Route path="/realname-authentication" component={RealNameAuthentication} />
//       <Route path="/tips" component={MiddlewareTips} />
//     </Router>
//   );
// }


const rootRoute = {
  path: '/home',
  indexRoute: {
    getComponent(nextState, cb) {
      require.ensure([], (require) => {
        cb(null, require('./routes/Wallet'))
      }, 'Wallet')
    },
  },
  childRoutes: [{
    path: '/amazon-bank-accounts',
    getComponent(nextState, cb) {
      require.ensure([], (require) => {
        cb(null, require('./routes/AmazonBankManagement'))
      }, 'AmazonBankManagement')
    }
  },
    {
      path: '/withdraw',
      getComponent(nextState, cb) {
        require.ensure([], (require) => {
          cb(null, require('./routes/Withdraw'))
        }, 'WithdrawWizard')
      }
    },
    {
      path: '/withdraw-bank-accounts',
      getComponent(nextState, cb) {
        require.ensure([], (require) => {
          cb(null, require('./routes/WithdrawBankManagement'))
        }, 'WithdrawBankManagement')
      }
    }, {
      path: '/withdraw-bank-accounts/new',
      getComponent(nextState, cb) {
        require.ensure([], (require) => {
          cb(null, require('./routes/NewWithdrawBankAccount'))
        }, 'NewWithdrawBankAccount')
      }
    }, {
      path: '/realname-authentication',
      getComponent(nextState, cb) {
        require.ensure([], (require) => {
          cb(null, require('./routes/RealNameAuthentication'))
        }, 'RealNameAuthentication')
      }
    }, {
      path: '/tips',
      getComponent(nextState, cb) {
        require.ensure([], (require) => {
          cb(null, require('./routes/MiddlewareTips'))
        }, 'MiddlewareTips')
      },
    }, {
      path: '/security',
      getComponent(nextState, cb) {
        require.ensure([], (require) => {
          cb(null, require('./routes/Security'))
        }, 'Security')
      },
    },
  ],
}


function RouterConfig({ history }) {
  return (<Router history={history} routes={rootRoute} />);
}

export default RouterConfig;
