import fetch from 'dva/fetch';

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

/**
 * Requests a URL, returning a promise.
 * options:{
 *  method: 'get'/'post'
 * data: {} post请求参数
 * }
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export default async function request(url, options) {
  console.log(url);
  const type = options && options.method ? options.method.toUpperCase() : 'GET';
  const params = options && options.data ? options.data : {};
  let requestConfig = {
        credentials: 'include', //发送cookie数据
        method: type,
        headers: {
          "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzdXBwb3J0QHRyYW5zZmVyLWV4cHJlc3MubmV0Iiwicm9sZXMiOjEyNywiaWF0IjoxNDg2MDQwOTAxfQ.j75AQ-QFMiPib3p-ChTU7aXNcSoWBSWNDpoVyhIUIbU",
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        mode: 'cors',
        cache: 'force-cache'
  };

  if (type == 'POST') {
    Object.defineProperty(requestConfig, 'body', {
      value: JSON.stringify(params)
    });
  } else if (type == 'PUT') {
    Object.defineProperty(requestConfig, 'body', {
      value: JSON.stringify(params)
    });
  }

  const response = await fetch(url, requestConfig);
   // console.log(response);
   // console.log("kokokok");
  checkStatus(response);

  const data = await response.json();
  console.log("----")
  const ret = {
    data,
    headers: {},
  };

  if (response.headers.get('x-total-count')) {
    ret.headers['x-total-count'] = Number(response.headers.get('x-total-count'));
  }

  return ret;
}


