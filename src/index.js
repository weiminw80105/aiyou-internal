import dva from 'dva';
import createLoading from 'dva-loading';
import { message } from 'antd'
import './index.css';
import { useRouterHistory } from 'dva/router';
import { createHashHistory } from 'history';

// 1. Initialize
// const app = dva();
const app = dva({
  onError(error) {
    // console.log(error.response.json())
    error.response.json().then((e) =>{
      message.error(e.message);
    },
    );
  },
  history: useRouterHistory(createHashHistory)({ queryKey: false }),
});

// 2. Plugins
app.use(createLoading());
// app.use({});

// 3. Model
app.model(require('./models/users'));
app.model(require('./models/wallet'));
app.model(require('./models/withdraws'));
app.model(require('./models/realNameAuthentication'));
app.model(require('./models/banks'));
app.model(require('./models/amazonBankAccounts'));
app.model(require('./models/security'));

// 4. Router
app.router(require('./router'));

// 5. Start
app.start('#root');

