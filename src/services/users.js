import request from '../utils/request';
import { PAGE_SIZE } from '../constants';

export function fetch({ page }) {
  return request(`/transfer-express-services/api/users?_page=${page}&_limit=${PAGE_SIZE}`);
}

export function fetchUserView({ userId }) {
  return request(`/transfer-express-services/api/users/${userId}/view`, {
    method: 'get',
    headers: {
      Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzdXBwb3J0QHRyYW5zZmVyLWV4cHJlc3MubmV0Iiwicm9sZXMiOjEyNywiaWF0IjoxNDg2MDQwOTAxfQ.j75AQ-QFMiPib3p-ChTU7aXNcSoWBSWNDpoVyhIUIbU',
      Accept: 'application/json',
    },
  });
}

export function setTransPassword({ userId, transactionPassword,verificationCode }) {
  return request(`/transfer-express-services/api/users/${userId}/daccounts`, {
    method: 'post' ,
    data: {
      transactionPassword,
      verificationCode,
    },
  });
}
