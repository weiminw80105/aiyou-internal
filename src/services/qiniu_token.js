import request from '../utils/request';

export function fetchToken() {
  console.info("---")
  return request(`/transfer-express-services/api/qiniu/upload/token`, {
    method: 'get',
  });
}
