import request from '../utils/request';

export function fetchUserTransactions({ userId }) {
  return request(`/transfer-express-services/api/users/${userId}/transactions`, {
    method: 'get',
    headers: {
      Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzdXBwb3J0QHRyYW5zZmVyLWV4cHJlc3MubmV0Iiwicm9sZXMiOjEyNywiaWF0IjoxNDg2MDQwOTAxfQ.j75AQ-QFMiPib3p-ChTU7aXNcSoWBSWNDpoVyhIUIbU',
      Accept: 'application/json',
    },
  });
}

export function caculateWithdrawFee({ userId, fromAmount, fromCurrency, toCurrency }) {
  return request(`/transfer-express-services/api/users/${userId}/withdraws/fee`, {
    method: 'POST',
    data: { fromAmount, fromCurrency, toCurrency },
  });
}

export function submitWithdraw({
                                 userId, userDigitalAccountId, bankAccount, amount, currency,
                                 transPasswd,
                               }) {
  const error = request(`/transfer-express-services/api/udas/${userDigitalAccountId}/withdraws`, {
    method: 'POST',
    data: {
      userId, userDigitalAccountId, bankAccount, amount, currency,
      transPasswd,
    },
  });
  return error;
  // return error.catch((error)=>{
  //   console.info("1-1-1-1")
  //   console.info(error)
  //   console.info("1-1-1-1")
  //   error.response.then((fff)=>{
  //     console.debug(fff)
  //   })
  // });
}
