import request from '../utils/request';

/**
 * 获取亚马逊店铺的银行账户
 * @param userId
 * @returns {Object}
 */
export function fetchAmzBanks({ userId }) {
  return request(`transfer-express-services/api/users/${userId}/amazons`, {
    method: 'get',
    headers: {
      Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzdXBwb3J0QHRyYW5zZmVyLWV4cHJlc3MubmV0Iiwicm9sZXMiOjEyNywiaWF0IjoxNDg2MDQwOTAxfQ.j75AQ-QFMiPib3p-ChTU7aXNcSoWBSWNDpoVyhIUIbU',
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
}

/**
 * 申请亚马逊店铺账户
 * @param userId
 * @returns {Object}
 */
export function applyAmzBank({ userId, entityName, sellerId, accessId, secretKey }) {
  console.log(JSON.stringify({
    entityName,
    sellerId,
    accessId,
    secretKey,
  }));
  return request(`/transfer-express-services/api/users/${userId}/amazon-legal-entities`, {
    method: 'post',
    headers: {
      Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzdXBwb3J0QHRyYW5zZmVyLWV4cHJlc3MubmV0Iiwicm9sZXMiOjEyNywiaWF0IjoxNDg2MDQwOTAxfQ.j75AQ-QFMiPib3p-ChTU7aXNcSoWBSWNDpoVyhIUIbU',
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    data:{ userId, entityName, sellerId, accessId, secretKey }

  });
}
