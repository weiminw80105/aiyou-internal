import request from '../utils/request';

export function fetchVerificationCode(userId) {
  return request(`/transfer-express-services/api/users/${userId}/validationRequest`, {
    method: 'post',
  });
}

export function verificationMobile(userId,mobile) {
  return request(`/transfer-express-services/api/verifications?userId=${userId}&mobile=${mobile}`, {
    method: 'post',
  });
}
