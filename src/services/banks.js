import request from '../utils/request';

export function fetchUserBanks({ userId }) {
  return request(`/transfer-express-services/api/users/${userId}/banks?currency=1`, {
    method: 'get',
    headers: {
      Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzdXBwb3J0QHRyYW5zZmVyLWV4cHJlc3MubmV0Iiwicm9sZXMiOjEyNywiaWF0IjoxNDg2MDQwOTAxfQ.j75AQ-QFMiPib3p-ChTU7aXNcSoWBSWNDpoVyhIUIbU',
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
}

export function createUserBank(userWithdrawBankRegistration) {
  // userWithdrawBankRegistration.userId = 164;
  return request(`/transfer-express-services/api/users/${userWithdrawBankRegistration.userId}/bankAccounts`, {
    method: 'post',
    headers: {
      Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzdXBwb3J0QHRyYW5zZmVyLWV4cHJlc3MubmV0Iiwicm9sZXMiOjEyNywiaWF0IjoxNDg2MDQwOTAxfQ.j75AQ-QFMiPib3p-ChTU7aXNcSoWBSWNDpoVyhIUIbU',
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    data:userWithdrawBankRegistration,
  });
}

export function fetchUserLegalEntity(userId) {
  return request(`/transfer-express-services/api/users/${userId}/legal-entities/available`, {
    method: 'get',
  });
}

export function validationRequest(userId) {
  return request(`/transfer-express-services/api/users/${userId}/validationRequest`, {
    method: 'post',
    headers: {
      Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzdXBwb3J0QHRyYW5zZmVyLWV4cHJlc3MubmV0Iiwicm9sZXMiOjEyNywiaWF0IjoxNDg2MDQwOTAxfQ.j75AQ-QFMiPib3p-ChTU7aXNcSoWBSWNDpoVyhIUIbU',
    },
  });
}
