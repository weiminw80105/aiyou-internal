import request from '../utils/request';

export function submitAuthtication({ userId, entityName, identityNumber, identityFiles, registrationLocation = 'CN', legalPersonFiles,mobile, verificationCode,type}) {
  console.log(type);
  return request(`/transfer-express-services/api/aiyou/users/${userId}/authentication`, {
    method: 'post',
    data: {
      entityName,
      identityNumber,
      userId,
      identityFiles,
      legalPersonFiles,
      registrationLocation,
      mobile,
      verificationCode,
      type,
    },
  });
}
