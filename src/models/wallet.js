import * as usersService from '../services/users';
import * as transactionService from '../services/transactions';
export default {
  namespace: 'wallet',
  state: {
    userInfo: null
  },
  reducers: {//mutations
    saveUserInfo(state, { payload: { data: userInfo } }) {
      sessionStorage.setItem('userInfo', JSON.stringify(userInfo))
      return { ...state, userInfo };
    },
    saveUserTransactions(state, { payload: { data: userTransactions } }) {
      return { ...state, userTransactions };
    },
  },
  effects: { //actions
    *fetchUserView({ payload: { userId } }, { call, put }) {
      const { data, headers } = yield call(usersService.fetchUserView, { userId });
      yield put({ type: 'saveUserInfo', payload: { data } });
    },
    *fetchUserTransactions({ payload: { userId } }, { call, put }) {
      const { data, headers } = yield call(transactionService.fetchUserTransactions, { userId });
      yield put({ type: 'saveUserTransactions', payload: { data } });
    },

    *setTransPassword({ onComplete, payload: { userId, transactionPassword, verificationCode } }, { call, put }) {

      const { data, headers } = yield call(usersService.setTransPassword, {
        userId,
        transactionPassword,
        verificationCode,
      });
      yield put({
        type:'fetchUserView',
        payload:{userId}
      });
    },

  },
  subscriptions: {
    setup({ dispatch, history }){
      history.listen((location) => {

        // dispatch({ type: 'fetchUserView', payload: query });
        if (location.pathname === '/home') {
          dispatch({ type: 'fetchUserView', payload: location.query });
          dispatch({ type: 'fetchUserTransactions', payload: location.query });
        }
      });
    },

    // setup({ dispatch, history }) {
    // return history.listen(({ pathname, query }) => {
    //     if (pathname === '/users') {
    //         dispatch({ type: 'fetchUserView', payload: query });
    //     }
    // });

    // },
  },
};
