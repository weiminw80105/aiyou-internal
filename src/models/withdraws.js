import * as transactionService from '../services/transactions';
import * as bankService from '../services/banks';

export default {
  namespace: 'withdraw',
  state: {
    userBanks: [],
    withdrawTransaction: null,
  },
  reducers: {//mutations
    saveUserBanks(state, { payload: { data: userBanks } }) {
      return { ...state, userBanks };
    },
    saveWithdrawResponse(state, { payload: { data: withdrawTransaction,error } }) {
      return { ...state, withdrawTransaction };
    },
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
  },
  effects: { //actions
    *fetchUserBanks({ payload: { userId } }, { call, put }) {
      const { data, headers } = yield call(bankService.fetchUserBanks, { userId });
      yield put({ type: 'saveUserBanks', payload: { data } });
    },
    *submitWithdrawRequest(
      { onError, payload:
        { userId, userDigitalAccountId, bankAccount, amount, currency, transPasswd } }
      , { call, put }){
      // console.info({ userId, userDigitalAccountId, bankAccount, amount, currency, transPasswd })
      try{
        const { data } = yield call(transactionService.submitWithdraw, {
          userId,
          userDigitalAccountId,
          bankAccount,
          amount,
          currency,
          transPasswd,
        });
        yield put({ type: 'saveWithdrawResponse', payload: { data } });
      } catch (e) {
        e.response.json().then((error) => {
          onError(error);
        });

      }

    }

  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname, query }) => {
        if (pathname === '/withdraw') {
          var userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
          dispatch({ type: 'fetchUserBanks', payload: { userId: userInfo.id } });
          dispatch({ type: 'updateState', payload: { withdrawTransaction: null } });
        }
      });
    },
  },
};
