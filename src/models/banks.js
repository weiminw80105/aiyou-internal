import { routerRedux } from 'dva/router';
import * as banksService from '../services/banks';
import * as verificationService from '../services/verification';

export default {
  namespace: 'banks',
  state: {
    list: [],
  },
  reducers: {
    save(state, { payload: { data: list } }) {
      return { ...state, list };
    },
    saveUserLegalEntity(state, { payload: { data: userLegalEntities } }) {
      return { ...state, userLegalEntities };
    },

  },
  effects: {
    * fetch({ payload: userId }, { call, put }) {
      const { data } = yield call(banksService.fetchUserBanks, { userId });
      yield put({
        type: 'save',
        payload: {
          data,
        },
      });
    },
    * fetchUserLegalEntity({ payload: userId }, { call, put }) {
      const { data } = yield call(banksService.fetchUserLegalEntity, userId);
      yield put({
        type: 'saveUserLegalEntity',
        payload: {
          data,
        },
      });
    },
    * create({ onError, payload: values }, { call, put }) {
      try {
        yield call(banksService.createUserBank, values);
        yield put(routerRedux.push({
          pathname: '/withdraw-bank-accounts',
        }));
      } catch (e) {
        console.log(e.response);
        e.response.json().then((error) => {
          onError(error);
        });
      }
    },
    * remove({ payload: id }, { put }) {
      // TODO
      yield put({ type: 'reload' });
    },
    * reload(action, { put }) {
      yield put({ type: 'fetch' });
    },
    * fetchVerificationCode({ payload: { userId } }, { call, put }) {
      yield call(verificationService.fetchVerificationCode, userId);
      // yield put({ type: 'reload' });
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
        if (pathname === '/withdraw-bank-accounts') {
          dispatch({ type: 'fetch', payload: userInfo.id });
          // dispatch({type: 'fetchUserLegalEntity',payload:164});
        } else if (pathname === '/withdraw-bank-accounts/new') {
          // dispatch({ type: 'fetch' });
          dispatch({ type: 'fetchUserLegalEntity', payload: userInfo.id });
        }
      });
    },
  },
};
