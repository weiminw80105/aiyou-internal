import * as amazonBankAccountService from '../services/amazons';

export default {
  namespace: 'amazonBankAccounts',
  state: {
    amazonBankAccounts: [],
    total: null,
    userId: null,
  },
  reducers: {//mutations
    save(state, { payload: { data: amazonBankAccounts, total, userId } }) {
      return { ...state, amazonBankAccounts, total, userId };
    },
  },
  effects: { //actions
    *fetchAmzBanks({ payload: { userId = 164 } }, { call, put }) {
      const { data, headers } = yield call(amazonBankAccountService.fetchAmzBanks, { userId });
      yield put({ type: 'save', payload: { data, total: parseInt(data.length, 10), userId } });
    },
    *remove({ payload: id }, { call, put }) {
      yield call(amazonBankAccountService.remove, id);

      // yield put({ type: 'reload' });
    },
    *applyAmzBanks({onComplete, payload: { userId, entityName, sellerId, accessId, secretKey } }, { call, put }) {

      const { data, headers } = yield call(amazonBankAccountService.applyAmzBank, { userId, entityName, sellerId, accessId, secretKey });
      // yield put({ type: 'reload' });
      yield put({ type: 'fetchAmzBanks',payload:{userId:data.id} });
      onComplete();
    },
    *reload(action, { put, select }) {
      const page = yield select(state => state.users.page);
      yield put({ type: 'fetchAmzBanks', payload: { page } });

    },

  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname, query }) => {
        if (pathname === '/amazon-bank-accounts') {
          const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
          dispatch({ type: 'fetchAmzBanks', payload: { userId: userInfo.id }, });
        }
      });
    },

  },
};
