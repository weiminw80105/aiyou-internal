import { setTransPassword } from "../services/users";
import { fetchVerificationCode } from '../services/verification';
import {message} from 'antd';

export default {
  namespace: 'security',
  state: {
    modalVisible: false,
  },
  reducers: {//mutations
    save(state, { payload }) {
      return { ...state, ...payload };
    },

  },
  effects: { //actions
    * showDialog({ payload }, { put }){

      yield put({ type: 'save', payload: { modalVisible: true } });
    },
    * setTransPassword({ payload }, { call, put }){
      // console.log(payload);
      const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
      const { data, headers } = yield call(setTransPassword, {
        userId: userInfo.id,
        transactionPassword: payload.password,
        verificationCode: payload.verificationCode,
      });
      message.success('修改支付密码成功');
      yield put({ type: 'save', payload: { modalVisible: false } });
    },
    * cancelSetTransPassword({ payload }, { put }){
      yield put({ type: 'save', payload: { modalVisible: false } });
    },

    * fetchVerificationCode({ payload}, { call, put }) {
      const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
      yield call(fetchVerificationCode, userInfo.id);
    },

  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname, query }) => {
        if (pathname === '/security') {
          dispatch({ type: 'save' });
        }
      });
    },

  },
};
