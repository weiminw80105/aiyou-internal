import * as uploadTokenService from '../services/qiniu_token';
import * as usersService from '../services/users';
import { routerRedux } from 'dva/router';
import * as realNameAuthenticationService from "../services/realNameAuthentication.js";
export default {
  namespace: 'realNameAuthentication',
  state: {},
  reducers: {
    save(state, { payload: { data: token } }) {
      return { ...state, token };
    },
    init(state, { payload: {data:userInfo } }) {
      return { ...state,userInfo };
    },
  },
  effects: {
    *fetchUploadToken({},{ call, put }) {
      const { data } = yield call(uploadTokenService.fetchToken, { });
      yield put({ type: 'save', payload: { data } });
    },
    *fetchUserView({ payload: { userId } }, { call, put }) {
      const { data, headers } = yield call(usersService.fetchUserView, { userId });
      // yield put({ type: 'saveUserInfo', payload: { data } });
      sessionStorage.setItem('userInfo', JSON.stringify(data));
      yield put({ type: 'init', payload: { data } })
    },
    *submitAuthtication({ payload: { values } }, { call, put }) {
      console.log(values)
      const { data, headers } = yield call(realNameAuthenticationService.submitAuthtication, {...values});
      yield put(routerRedux.push({
        pathname: '/tips',
      }));
    },
  },
  subscriptions: {
     setup({ dispatch, history }) {
        return history.listen(({ pathname, query }) => {
          if (pathname === '/realname-authentication') {
            dispatch({ type: 'fetchUploadToken' });
            dispatch({ type: 'fetchUserView', payload: query });
          }
        });
      },
  },
};
