import * as usersService from "../services/users";
import * as verificationService from "../services/verification";
export default {
  namespace: 'users',
  state: {
    list: [],
    total: null,
  },
  reducers: {//mutations
    save(state, { payload: { data: list, total, page } }) {
      return { ...state, list, total, page };
    },
    saveUserInfo(state, { payload: { data: userInfo } }) {
      sessionStorage.setItem('userInfo', JSON.stringify(userInfo))
      return { ...state, userInfo };
    },
  },
  effects: { //actions
    *fetch({ payload: { page } }, { call, put }) {
      const { data, headers } = yield call(usersService.fetch, { page });
      yield put({ type: 'save', payload: { data, total: headers['x-total-count'] } });
    },
    *fetchUserView({ payload: { userId } }, { call, put }) {
      const { data, headers } = yield call(usersService.fetchUserView, { userId });
      yield put({ type: 'saveUserInfo', payload: { data } });
    },
    *setTransPassword({ onComplete, payload: { userId, transactionPassword, verificationCode } }, { call, put }) {
      const { data, headers } = yield call(usersService.setTransPassword, {
        userId,
        transactionPassword,
        verificationCode,
      });
      yield put({
        type:'fetchUserView',
        payload:{userId}
      })
      // onComplete();
    },
    * fetchVerificationCode({ payload: { userId } }, { call, put }) {
      yield call(verificationService.fetchVerificationCode, userId);
      // yield put({ type: 'reload' });
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname, query }) => {
        if (pathname === '/users') {
          dispatch({ type: 'fetch', payload: query });
        }
      });
    },
  },
};
