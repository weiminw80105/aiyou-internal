import React from "react";
import { connect } from "dva";
import AmazonBankAccounts from "../components/AmazonBankManagement/AmazonBankAccounts";
import NewAmazonBankAccount from "../components/AmazonBankManagement/NewAmazonBankAccount";
import MainLayout from "../components/MainLayout/MainLayout";
import styles from "./AmazonBankManagement.css";
import {Button, Modal} from 'antd'

class AmazonBankManagement extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      newAmzAccount: false,
    }
  }

  saveNewAccount(data){
    console.log(data);
    const { dispatch } = this.props;
    const userInfo = JSON.parse(sessionStorage.getItem('userInfo'))
    var _this = this;
    dispatch({
      type: 'amazonBankAccounts/applyAmzBanks',
      payload: { userId: userInfo.id, entityName: data.entityName, sellerId: data.sellerId, accessId: data.accessId, secretKey: data.secretKey },
      onComplete(){
        _this.setState({
          newAmzAccount: false,
        })

      }
    });
  }
  toggle(){
    this.setState({
      newAmzAccount: !this.state.newAmzAccount,
    })
  }
  render() {
    var content = <AmazonBankAccounts
      clickNewAccountButton={this.toggle.bind(this)}
    />
    if(this.state.newAmzAccount||this.props.newAmzAccount){
      content=
        <NewAmazonBankAccount
          onSubmit={this.saveNewAccount.bind(this)}
          back={this.toggle.bind(this)}/>
    }
    return (
      <MainLayout>
        {content}
      </MainLayout>
    )
  }
}

function mapStateToProps(state) {
  const { newAmzAccount } = state.amazonBankAccounts;
  return {
    loading: state.loading.models.amazonBankAccounts,
    newAmzAccount,
  };
}

export default connect(mapStateToProps)(AmazonBankManagement);
