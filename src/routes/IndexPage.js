import React from 'react';
import { connect } from 'dva';
import { Button, Col, Form, Input, Row, Select } from 'antd';
import styles from './IndexPage.css';
import Users from './Users';
const FormItem = Form.Item;
const Option = Select.Option;

const selectAfter = (
  <Select defaultValue="按爱邮网id搜索" style={{ width: 120 }}>
    <Option value="按爱邮网id搜索">按爱邮网id搜索</Option>
    <Option value="按邮箱搜索">按邮箱搜索</Option>
    <Option value="按手机号搜索">按手机号搜索</Option>
  </Select>
);

class IndexPage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    return (
      <div className={styles.normal}>
        <Row type="flex" justify="space-around" align="top" offset="10px" style={{ overflow: 'hidden' }}>
          <Col span="16" className={styles.bgUnNormal}>
            <div className={styles.ratioWraper}>
              <h2 className={styles.titleLevel2}>注册用户：18625<span style={{ fontSize: '12px' }}>(人)</span></h2>
              <div >
                <h3 className={styles.titleLevel3}>用户搜索</h3>
                <div className="ant-row-flex">
                  <div className="ant-col-18">
                    <Input addonAfter={selectAfter}/>
                  </div>
                  <Button type="primary" style={{ marginLeft: 20 }}>搜索</Button>
                </div>
                <Users></Users>
              </div>
            </div>
          </Col>
          <Col span="8" className={styles.bgColor}>
            <div className={styles.loginOut}>安全退出</div>
            <h3 className={styles.ratioTitle}>费率设置</h3>
            <div className={styles.ratioStyles}>爱邮网ID:123456</div>
            <div className={styles.ratioStyles}>费率</div>
            <div className={styles.ratioStyles}>修改费率</div>

          </Col>
        </Row>
        <Users></Users>
      </div>
    );
  }
}


export default connect()(IndexPage);
