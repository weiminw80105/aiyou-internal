import React from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router'
import PageTitle from '../components/Commons/PageTitle';
import styles from './SecurityCenter.css';
import MainLayout from '../components/MainLayout/MainLayout';
import { Button, Row, Col, Card, message } from 'antd';
import RetrievePassword from '../components/Security/RetrievePassword';

function Security({ dispatch, location, security }) {
  // if (security.updateTransPwdSuccess) {
  //   message.success('修改支付密码成功');
  // }


  const onSubmitPassword = (formData) => {
    dispatch({
      type: 'security/setTransPassword',
      payload: { ...formData },
    });
  };
  const showDialog = () => {
    dispatch({
      type: 'security/showDialog',
    });
  }

  const onCancel = () => {
    dispatch({
      type: 'security/cancelSetTransPassword',
    });
  };
  const onFetchVerificationCode = () => {
    dispatch({
      type: 'security/fetchVerificationCode',
    });
  }
  const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
  return (
    <MainLayout location={location}>
      <div>
      <PageTitle title="安全中心">
        <Link to={"/home?userId="+userInfo.id}>
          <Button
            type="default" shape="circle" icon="rollback" size="default"
          ></Button>
        </Link>

      </PageTitle>

      <RetrievePassword
        onFetch={onFetchVerificationCode}
        visible={security.modalVisible}
        onSubmit={onSubmitPassword}
        onCancel={onCancel}
      />

      <Row type="flex" justify="center" align="middle"  className={styles.normal}>
        <Col span={24} className="text-center">
          <Card >
            <label>支付密码:</label><Button style={{marginLeft:"64px"}} onClick={showDialog}>修改密码</Button>
          </Card>
        </Col>
      </Row>
      </div>
    </MainLayout>
  );
}

export default connect(({ security, loading }) => ({ security, loading }))(Security);
