import React from 'react';
import { connect } from 'dva';
import styles from './MiddlewareTips.css';

function MiddlewareTips() {
    return (
        <div className = { styles.normal } >
            请您耐心等待, 实名认证会在24小时内完成， 审核通过后方可使用亚马逊收款服务（ 我们会以短信的形式通知您， 请注意查看)
        </div>
    );
}

export default connect()(MiddlewareTips);
