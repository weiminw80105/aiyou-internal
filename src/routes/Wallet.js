import React from 'react';
import { Spin } from 'antd'
import { connect } from 'dva';
import styles from './Wallet.css';
import NewPasswordForm from '../components/Wallet/NewPasswordForm';
import WaitForAuthentication from '../components/Wallet/WaitForAuthentication';
import UserView from '../components/Wallet/UserView';
import UserTransactions from '../components/Wallet/Transactions';
import MainLayout from '../components/MainLayout/MainLayout';

class Wallet extends React.Component {

  constructor(props){
    super(props)

    // this.setState({
    //   userInfo,
    // })
  }
  render() {
    const { userInfo } = this.props;
    console.log(this.props)
    if(userInfo==null){
      return (
        <MainLayout>
          <Spin style={{width:"100%",height:"100vh",marginTop:"64px"}} spinning={!userInfo}></Spin>
        </MainLayout>
      )
    }

    // const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    let dialog = null;
    if(userInfo.status<2){
      dialog = <WaitForAuthentication/>

    } else {
      if(!userInfo.hasTransPassword){
        dialog = <NewPasswordForm /> ;
      }
    }

    return (
      <MainLayout>
        {dialog}
        <UserView className={styles.userinfo} userInfo={this.props.userInfo} />
        <div className={styles.transactions}>
          <UserTransactions
            loading={this.props.loading}
            transactions={this.props.userTransactions} />
        </div>
      </MainLayout>
    );
  }
}

function mapStateToProps(state) {
  const { userInfo, userTransactions } = state.wallet;
  console.log(userInfo)
  return {
    loading: state.loading.models.wallet,
    userInfo,
    userTransactions,
  };
}

export default connect(mapStateToProps)(Wallet);
