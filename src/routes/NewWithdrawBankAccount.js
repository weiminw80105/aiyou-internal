import React from 'react';
import { connect } from 'dva';
import NewBankAccountForm from '../components/WithdrawBankManagement/NewBankAccountForm';
import MainLayout from '../components/MainLayout/MainLayout';

class NewWithdrawBankAccount extends React.Component {
  submitNewWithdrawBank(data,errorCallback) {
    const { dispatch } = this.props;
    const _this = this;
    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    dispatch({
      type: 'banks/create',
      payload: Object.assign({},data,{userId:userInfo.id}),
      onError(error){
        errorCallback(error);
      }
    });
  }

  render() {
    return (
      <MainLayout location={this.props.location}>
        <NewBankAccountForm
          error
          userLegalEntities={this.props.userLegalEntities ? this.props.userLegalEntities : []}
          onSubmit={this.submitNewWithdrawBank.bind(this)}
        />
      </MainLayout>
    );
  }

}

function mapStateToProps(state) {
  const { userLegalEntities } = state.banks;
  console.info(userLegalEntities)
  return {
    userLegalEntities,
  };
}

export default connect(mapStateToProps)(NewWithdrawBankAccount);
