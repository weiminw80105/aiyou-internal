import React from 'react';
import { connect } from 'dva';
import BankAccounts from '../components/WithdrawBankManagement/BankAccounts';
import MainLayout from '../components/MainLayout/MainLayout';

function WithdrawBankManagement({ location }) {
  return (
    <MainLayout location={location}>
      <div>
        <BankAccounts />
      </div>
    </MainLayout>
  );
}

export default connect()(WithdrawBankManagement);
