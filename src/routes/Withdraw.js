import React from 'react';
import { connect } from 'dva';
import md5 from 'md5';
import WithdrawEdit from '../components/WithdrawWizard/WithdrawEdit';
import WithdrawCompleted from '../components/WithdrawWizard/WithdrawCompleted';
import MainLayout from '../components/MainLayout/MainLayout';
import { Alert } from 'antd';
class WithdrawWizard extends React.Component {
  state = {
    successed: false,
    errorMessage: '',
  }

  submitWithdrawRequest(formData) {
    const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
    const { dispatch } = this.props;
    const _this = this;
    dispatch({
      type: 'withdraw/submitWithdrawRequest',
      payload: {
        userId: userInfo.id,
        userDigitalAccountId: userInfo.digitalAccountId,
        bankAccount: formData.bankAccount,
        amount: formData.amount,
        currency: 1,
        transPasswd: md5(formData.transPasswd),
      },
      onError(error) {
        _this.setState({
          errorMessage: error.message,
        });
      },
    });
  }
  render() {
    let content = <WithdrawEdit
      initData={{ error: this.state.errorMessage }}
      userBanks={this.props.userBanks ? this.props.userBanks : []}
      onSubmit={this.submitWithdrawRequest.bind(this)}
    />
    if (this.props.withdrawTransaction) {
      content = <WithdrawCompleted />;
    }
    return (
      <MainLayout>
        {content}
      </MainLayout>

    );
  }
}

function mapStateToProps(state) {
  const { userBanks, withdrawTransaction } = state.withdraw;
  return {
    loading: state.loading.models.withdraw,
    userBanks,
    withdrawTransaction,
  };
}
export default connect(mapStateToProps)(WithdrawWizard);
