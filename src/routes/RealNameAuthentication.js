import React from "react";
import { connect } from "dva";
import {routerRedux} from 'dva/router'
import { Form, Radio} from "antd";
import styles from "./RealNameAuthentication.css";
import { submitAuthtication } from "../services/realNameAuthentication.js";
// import AuthenticationForm from "../components/RealNameAuthentication/AuthenticationForm.js";
import PersonAuthenticationForm from '../components/RealNameAuthentication/PersonAuthenticationForm';
import CompanyAuthenticationForm from '../components/RealNameAuthentication/CompanyAuthenticationForm';
import { browserHistory } from "react-router";
const FormItem = Form.Item;
const RadioGroup = Radio.Group;

class RealNameAuthentication extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: 'person',
      registrationLocationValue: 'CN',
      isSpinLoading: false
    };
    // this.tips = '请您耐心等待,实名认证会在24小时内完成，审核通过后方可使用亚马逊收款服务（我们会以邮件的形式通知您，请注意查看）'
    // this.delayTime = 50000000
  }

  onChangeRadioState = (e) => {
    this.setState({
      value: e.target.value,
    });
    // this.props.form.resetFields();
  }

  handleSubmitForm(values) {
    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    // this.handleSpinning()
    // submitAuthtication(values).then(data => {
    //   routerRedux.push({pathname:"/tips"})
    //
    //
    //     // browserHistory.push('/tips');
    //
    // }).catch(err => {
    //   console.log(err);
    // })
    const { dispatch } = this.props;
    dispatch({
      type:'realNameAuthentication/submitAuthtication',
      payload:{values},
    })
  }
  handleSpinning(){
    this.setState({
      isSpinLoading: true
    })
  }
  handleChangeFileUpload = (value) => {
    console.log(value);
  }

  routerWillLeave(nextLocation) {
    return '确认要离开？';
  }

  render() {
    var content = null;
    if(this.state.value==="person") {

      content = <PersonAuthenticationForm
        userInfo={this.props.userInfo}
        uptoken={this.props.token ? this.props.token.uptoken : ''}
        stateValue={this.state.value}
        onformSubmit={this.handleSubmitForm.bind(this)} />

    } else {
      content = <CompanyAuthenticationForm
        userInfo={this.props.userInfo}
        uptoken={this.props.token ? this.props.token.uptoken : ''}
        stateValue={this.state.value}
        onformSubmit={this.handleSubmitForm.bind(this)} />
    }
    return (
      <div className={styles.normal}>
        <div className={styles.authTitle}>实名认证</div>
        <div className={styles.authDesc}>如需使用亚马逊收款服务，请提交以下信息进行审核开通</div>
        <div className={styles.authOption}>
          <RadioGroup onChange={this.onChangeRadioState} value={this.state.value}>
            <Radio value="person">个人</Radio>
            <Radio value="enterprise">企业</Radio>
          </RadioGroup>
        </div>
        <div className={styles.authFormWarp}>
          {

            content
          }

        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { token, userInfo } = state.realNameAuthentication;
  return {
    loading: state.loading.global,
    token,
    userInfo,
  };
}
export default connect(mapStateToProps)(RealNameAuthentication);
