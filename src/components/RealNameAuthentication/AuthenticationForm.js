import React from "react";
import { Button, Form, Input, Radio } from "antd";
import SinglePictureUpload from "../Commons/SinglePictureUpload.jsx";
import styles from "../../routes/RealNameAuthentication.css";
const FormItem = Form.Item;
const RadioGroup = Radio.Group;

class AuthenticationForm extends React.Component {
  constructor(props) {
    super(props)
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        let queryParams = {};
        Object.keys(values).map((item, index) => {
          let tempItem = item.split("_");
          if (tempItem.length <= 2) {
            queryParams[tempItem[1]] = values[item];
          } else {
            if (this.props.stateValue == 'enterprise') {
              queryParams.legalPersonFiles = queryParams.legalPersonFiles + ',' + values[item];
            } else if (this.props.stateValue == 'person') {
              queryParams.identityFiles = queryParams.identityFiles + ',' + values[item];
            }
          }
        })
        this.props.onformSubmit(queryParams)
      }
    });
  }

  checkIdentifyNumber(rule, value, callback) {

  }

  handleChangeFileUpload = (value) => {
    console.log(value);
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };
    let authenticationForm = null;
    var stateValue = this.props.stateValue;
    if (stateValue == 'person') {
      return (<Form className="auth-form" onSubmit={this.handleSubmit}>
        <FormItem
          className={styles.authFormItem}
          label="爱邮网ID"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
         <span>{this.props.userInfo ? this.props.userInfo.email : ''}</span>
        </FormItem>
        <FormItem
          className={styles.authFormItem}
          label="姓名"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          {getFieldDecorator('person_entityName', {
            rules: [{ required: true, message: '请输入姓名!' }],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem
          className={styles.authFormItem}
          label="身份证号"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          {getFieldDecorator('person_identityNumber', {
            rules: [{ required: true, message: '请输入身份证号!' }],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem
          className={styles.authFormItem}
          label="身份证件照"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          <p>请确保所上传的照片文字清晰，小于2M</p>
        </FormItem>
        <FormItem
          className={styles.authFormItem}
          label="正面"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          {getFieldDecorator('person_identityFiles', {
            rules: [{ required: false, message: '请上传身份证正面!' }],
          })(
            <SinglePictureUpload
              uptoken={this.props.uptoken}
              onChange={this.handleChangeFileUpload.bind(this)}></SinglePictureUpload>
          )}
        </FormItem>
        <FormItem
          className={styles.authFormItem}
          label="反面"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          {getFieldDecorator('person_identityFiles_reverse', {
            rules: [{ required: false, message: '请上传身份证反面!' }],
          })(
            <SinglePictureUpload
              uptoken={this.props.uptoken}
              onChange={this.handleChangeFileUpload.bind(this)}></SinglePictureUpload>
          )}
        </FormItem>
        <FormItem
          className={styles.authFormItem}
          wrapperCol={{ span: 10, offset: 12 }}
        >
          <Button type="primary" htmlType="submit">
            提交
          </Button>
        </FormItem>
      </Form>)
    } else {
      return ( <Form className="auth-form" onSubmit={this.handleSubmit}>
        <FormItem
          className={styles.authFormItem}
          label="公司名"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          {getFieldDecorator('enterprise_entityName', {
            rules: [{ required: true, message: '请输入公司名!' }],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem
          className={styles.authFormItem}
          label="营业执照号"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          {getFieldDecorator('enterprise_identityNumber', {
            rules: [{ required: true, message: '请输入营业执照号!' }],
          })(
            <Input />
          )}
        </FormItem>
        <FormItem
          className={styles.authFormItem}
          label="营业执照"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          <p>请确保所上传的照片文字清晰，小于2M</p>
          {getFieldDecorator('enterprise_identityFiles', {
            rules: [{ required: true, message: '请上传营业执照!' }],
          })(
            <SinglePictureUpload
              uptoken={this.props.uptoken}
              onChange={this.handleChangeFileUpload.bind(this)}></SinglePictureUpload>
          )}
        </FormItem>
        <FormItem
          className={styles.authFormItem}
          label="法人身份证照"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          <p>请确保所上传的照片文字清晰，小于2M</p>
        </FormItem>
        <FormItem
          className={styles.authFormItem}
          label="正面"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          {getFieldDecorator('enterprise_legalPersonFiles', {
            rules: [{ required: true, message: '请上传法人身份证照正面!' }],
          })(
            <SinglePictureUpload
              uptoken={this.props.uptoken}
              onChange={this.handleChangeFileUpload.bind(this)}></SinglePictureUpload>
          )}
        </FormItem>
        <FormItem
          className={styles.authFormItem}
          label="反面"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          {getFieldDecorator('enterprise_legalPersonFiles_reverse', {
            rules: [{ required: true, message: '请上传法人身份证照反面!' }],
          })(
            <SinglePictureUpload
              uptoken={this.props.uptoken}
              onChange={this.handleChangeFileUpload.bind(this)}></SinglePictureUpload>
          )}
        </FormItem>
        <FormItem
          className={styles.authFormItem}
          wrapperCol={{ span: 10, offset: 12 }}
        >
          <Button type="primary" htmlType="submit">
            提交
          </Button>
        </FormItem>
      </Form>)
    }

  }
}

function mapStateToProps(state, ownProps) {
  return {

  };
}

export default Form.create()(AuthenticationForm);

// export default connect(mapStateToProps)(RealNameAuthentication);
