import React from "react";
import { Button, Form, Input, Radio, Row, Col } from "antd";
import SinglePictureUpload from "../Commons/SinglePictureUpload.jsx";
import styles from "../../routes/RealNameAuthentication.css";
import * as verificationCodeService from  '../../services/verification'
import CountDownButton from '../Commons/CountDownButton'

const FormItem = Form.Item;
const RadioGroup = Radio.Group;

class CompanyAuthenticationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isGetCode: false
    }
  }
  // 处理倒计时结束状态重置
  handleComplete = (e) => {
    this.setState({
      isGetCode: false
    });
  }
  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        const values = this.props.form.getFieldsValue();
        const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
        this.props.onformSubmit({
          userId:userInfo.id,
          entityName:values.entityName,
          identityNumber:values.identityNumber,
          identityFiles:values.identityFiles_1,
          legalPersonFiles:values.legalPersonFiles_1+","+values.legalPersonFiles_2,
          mobile:values.mobile,
          verificationCode:values.verificationCode,
          type:2,
        });
      }
    });
  }

  fetchVerificationCode(e,cb){
    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    this.props.form.validateFieldsAndScroll(["mobile"],{},(errors,values)=>{
      if(!errors){
        verificationCodeService.verificationMobile(userInfo.id,this.props.form.getFieldValue("mobile"));
        cb();
      }
    });
    // verificationCodeService.verificationMobile(userInfo.id,);
  }

  handleChangeFileUpload = (value) => {
    console.log(value);
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };
    let authenticationForm = null;
    let stateValue = this.props.stateValue;

    // const buttonText = 60;
    // let buttonCode = null;
    // if (this.state.isGetCode) {
    //   buttonCode = <CountDownButton size="default" value={buttonText} onComplete={this.handleComplete.bind(this)}></CountDownButton>
    // } else {
    //   buttonCode = <Button size="default" onClick={this.fetchVerificationCode.bind(this)}>获取手机验证码</Button>
    // }
    return (<Form className="auth-form" onSubmit={this.handleSubmit}>
      <FormItem
        className={styles.authFormItem}
        label="爱邮网ID"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >
        <span>{this.props.userInfo ? this.props.userInfo.email : ''}</span>
      </FormItem>
      <FormItem
        className={styles.authFormItem}
        label="公司名"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >
        {getFieldDecorator('entityName', {
          rules: [{ required: true, message: '请输入公司名!' }],
        })(
          <Input />
        )}
      </FormItem>
      <FormItem
        className={styles.authFormItem}
        label="统一社会信用号码"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >
        {getFieldDecorator('identityNumber', {
          rules: [{ required: true, message: '请输入统一社会信用号码!' }],
        })(
          <Input />
        )}
      </FormItem>

      <FormItem
        className={styles.authFormItem}
        label="企业营业执照"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >
        {getFieldDecorator('identityFiles_1', {
          rules: [{ required: true, message: '请上传企业营业执照!' }],
        })(
          <SinglePictureUpload
            uptoken={this.props.uptoken}
            onChange={this.handleChangeFileUpload.bind(this)}></SinglePictureUpload>
        )}
      </FormItem>

      <FormItem
        className={styles.authFormItem}
        label="联系人手机号码"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >
        {getFieldDecorator('mobile', {
          rules: [{ required: true, message: '联系人请输入手机号码!' }],
        })(
          <Input />
        )}
      </FormItem>


      <FormItem
        className={styles.authFormItem}
        label="验证码"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >


        <Row gutter={8}>
          <Col span={12}>
            {getFieldDecorator('verificationCode', {
              rules: [{ required: true, message: '请输入验证码!' }],
            })(
              <Input />
            )}
          </Col>
          <Col span={12}>
            {/*{buttonCode}*/}
            <CountDownButton size="default" onClick={this.fetchVerificationCode.bind(this)}>获取手机验证码</CountDownButton>
          </Col>
        </Row>

      </FormItem>



      <FormItem
        className={styles.authFormItem}
        label="法人身份证件照"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >
        <p>请确保所上传的照片文字清晰，小于2M</p>
      </FormItem>
      <FormItem
        className={styles.authFormItem}
        label="正面"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >
        {getFieldDecorator('legalPersonFiles_1', {
          rules: [{ required: true, message: '请上传身份证正面!' }],
        })(
          <SinglePictureUpload
            uptoken={this.props.uptoken}
            onChange={this.handleChangeFileUpload.bind(this)}></SinglePictureUpload>
        )}
      </FormItem>
      <FormItem
        className={styles.authFormItem}
        label="反面"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
      >
        {getFieldDecorator('legalPersonFiles_2', {
          rules: [{ required: true, message: '请上传身份证反面!' }],
        })(
          <SinglePictureUpload
            uptoken={this.props.uptoken}
            onChange={this.handleChangeFileUpload.bind(this)}></SinglePictureUpload>
        )}
      </FormItem>
      <FormItem
        className={styles.authFormItem}
        wrapperCol={{ span: 10, offset: 12 }}
      >
        <Button type="primary" htmlType="submit">
          提交
        </Button>
      </FormItem>
    </Form>);
  }
}

function mapStateToProps(state, ownProps) {
  return {};
}

export default Form.create()(CompanyAuthenticationForm);

// export default connect(mapStateToProps)(RealNameAuthentication);
