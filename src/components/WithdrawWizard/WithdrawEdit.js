import React from 'react';
import { Button, Form, Input, InputNumber, Select, Alert } from 'antd';
import * as transactionService from '../../services/transactions';
import PageTitle from '../Commons/PageTitle';
import styles from './WithdrawEdit.css';
import { Link } from 'dva/router'

const FormItem = Form.Item;
const Option = Select.Option

class WithdrawEdit extends React.Component {
  state = {
    withdrawFeeResponse: {},
    hasSubmited: this.props.initData.error || false,
  }

  onSubmit() {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        // let formData = this.props.form.getFieldsValue();

        const formData = this.props.form.getFieldsValue();
        this.props.onSubmit(formData);
        this.setState({
          hasSubmited:true,
        });

      };
    });
  }

  checkWithdrawAmount(rule, value, callback) {
    if (value) {
      const userInfo = JSON.parse(sessionStorage.getItem('userInfo'))
      console.log(userInfo)
      if (parseFloat(value) > parseFloat(userInfo.balance)) {
        callback('提现金额不能超过当前账户余额');
      } else if (parseFloat(value) <= 0) {
        callback('请输入正确的提现金额');
      } else if (parseFloat(value) <= 2) {
        callback('最低提现金额为不低于2美金');
      } else {
        callback();
      }
    } else {
      callback('请输入正确的提现金额');
    }

  }


  caculateWithdrawFee() {
    const formData = this.props.form.getFieldsValue();
    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    transactionService.caculateWithdrawFee(
      { userId:userInfo.id, fromAmount: formData.amount, fromCurrency: 2, toCurrency: 1 },
    ).then(({ data }) => {
      this.setState({
        withdrawFeeResponse: data,
      });
    });
  }


  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };
    const userBanksSelect = [];
    this.props.userBanks.map(function(item) {
      userBanksSelect.push(
        <Option value={'' + item.id} key={item.id}>
          {item.bankName} - {item.accountHolderName} ({item.accountNumber})
        </Option>);
    })
    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"))
    return (
      <div className={styles.normal}>
        <PageTitle title="提现申请">
          <Link to={"/home?userId="+userInfo.id}>
            <Button
              type="default" shape="circle" icon="rollback" size="default"
            ></Button>
          </Link>
        </PageTitle>
        <div >

        </div>


      <Form style={{marginTop:"32px"}} layout="horizontal" className={styles.tableWrap}>

        <FormItem
          {...formItemLayout}
          label="提现金额"
          required="rue"
          extra="最低提现金额为10美金"

        >
          {
            getFieldDecorator('amount', {
              initialValue: null,
              rules: [
                { type: 'number' },
                { validator: this.checkWithdrawAmount.bind(this) }
              ],
            })(<InputNumber min={10} max={2000000} onBlur={this.caculateWithdrawFee.bind(this)}/>)
          }
        </FormItem>

        <FormItem
          {...formItemLayout}
          label='提现账户'
          extra='人民币提现 - 支持境内个人,公司账户'

        >
          {
            getFieldDecorator('bankAccount', {
              rules: [
                { required: true, message: '提现银卡不能为空' }
              ]
            })(<Select>
              {userBanksSelect}

            </Select>)
          }
        </FormItem>

        <FormItem
          {...formItemLayout}
          label='支付密码'
          extra="请您输入由8位以上数字、字母、符号所组成的支付密码"
          required='true'
        >
          {
            getFieldDecorator('transPasswd', {
              initialValue: '',
              rules: [
                { required: true, message: '请填写支付密码' },
              ],
            })(<Input type="password" />)
          }
        </FormItem>


        <FormItem
          {...formItemLayout}
          label="到账金额"
        >
          <span>￥ {this.state.withdrawFeeResponse.toAmount}</span>
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="手续费"
        >
          <span>$ {this.state.withdrawFeeResponse.fee}</span>
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="当前汇率"
        >
          <span>{this.state.withdrawFeeResponse.rate}</span>
        </FormItem>

        <FormItem
          {...formItemLayout}
          label="到账时间"
          extra={ this.props.initData.error?<Alert
            showIcon="true"
            message={this.props.initData.error}
            type="error"
          />:""}
        >
          <span>24小时到账,节假日将顺延</span>
        </FormItem>

        <FormItem
          wrapperCol={{ span: 14, offset: 6 }}
        >
          <Button onClick={this.onSubmit.bind(this)} disabled={this.props.initData.error ? false : this.state.hasSubmited}>确认提现</Button>
        </FormItem>
      </Form>
      </div>
    );
  }
}


export default Form.create()(WithdrawEdit);
