import React from 'react';
import PageTitle from '../Commons/PageTitle';
import styles from './WithdrawCompleted.css';
import { connect } from 'dva';
import { Link } from 'dva/router';

import { Col, Row, Steps,Button } from 'antd';
const Step = Steps.Step;
class WithdrawCompleted extends React.Component {

  render() {
    var userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
    var withdrawData = this.props.withdrawData ? this.props.withdrawData.data : {};
    return (

      <div className={styles.normal}>
        <PageTitle title="提现申请">
          <Link to={"/home?userId="+userInfo.id}>
            <Button
              type="default" shape="circle" icon="rollback" size="default"
            ></Button>
          </Link>
        </PageTitle>
        <Row type="flex" justify="space-around" align="middle">
          <Col span={16} className={'text-center ' + styles.section}></Col>

        </Row>
        <Row type="flex" justify="center" align="middle">
          <Col span={16} className={'text-center ' + styles.section}><h3>提现申请提交成功,系统处理中</h3></Col>

        </Row>
        <Row type="flex" justify="center" align="middle">
          <Col span={16} className={'text-center ' + styles.section}>您可以在<Link to={"/home?userId="+userInfo.id}><span
            className="h5">'交易记录'</span></Link>中查看处理进度</Col>

        </Row>
        <Row type="flex" justify="center" align="middle">
          <Col span={16} className={styles.section}>
            <Steps>
              <Step title="申请提现" status="finished" description={withdrawData.createTime}></Step>
              <Step title="系统处理" status="process" description={withdrawData.createTime}></Step>
              <Step title="预计到账" status="wait" description="24小时后,节假日将顺延"></Step>
            </Steps>
          </Col>
        </Row>
      </div>



    );
  }
}


export default connect()(WithdrawCompleted);
