import React, { Component } from "react";
import { Button, Form, Input } from "antd";
import styles from "./NewAmazonBankAccount.css";
import PageTitle from "../Commons/PageTitle";

const FormItem = Form.Item;

class NewAmazonBankAccount extends Component {

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }


  onSubmit() {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const formData = this.props.form.getFieldsValue();
        this.props.onSubmit(formData);
      }
    });
  }

  render() {
    const { children } = this.props;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };

    return (

      <div className={styles.normal}>
        <div>
          <PageTitle title="亚马逊收款账号申请">
            <Button
              onClick={() => {
                this.props.back();
              }}
              type="primary" shape="circle" icon="rollback" size="default"
            />
          </PageTitle>
          <div className={styles.authFormWarp}>
            <Form horizontal onSubmit={this.okHandler}>
              <FormItem
                {...formItemLayout}
                label="店铺名称"
              >
                {
                  getFieldDecorator('entityName', {
                    rules:[
                      {required:true,message:"请输入店铺名"}
                    ]
                  })(<Input />)
                }
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="Seller ID"
              >
                {
                  getFieldDecorator('sellerId', {
                    rules:[
                      {required:true,message:"请输入SellerId"}
                    ]
                  })(<Input />)
                }
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="AWS Access ID"
              >
                {
                  getFieldDecorator('accessId', {
                    rules:[
                      {required:true,message:"请输入MWS Access ID"}
                    ]
                  })(<Input />)
                }
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="Secret Key"
              >
                {
                  getFieldDecorator('secretKey', {
                    rules:[
                      {required:true, message:"请输入MWS Secret Key"}
                    ]
                  })(<Input />)
                }
              </FormItem>
              <FormItem
                wrapperCol={{ span: 14, offset: 6 }}
              >
                <Button onClick={this.onSubmit.bind(this)}>提交</Button>
              </FormItem>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}

export default Form.create()(NewAmazonBankAccount);
