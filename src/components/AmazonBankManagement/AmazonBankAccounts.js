import React from 'react'
import { connect } from 'dva'
import { Link } from 'dva/router';
import { Table, Pagination, Popconfirm, Button, Icon } from 'antd';
import NewAmazonBankAccount from './NewAmazonBankAccount';
import DelAmazonBankAccount from './DelAmazonBankAccount';
import styles from './AmazonBankAccounts.css';
import PageTitle from '../Commons/PageTitle';


class AmazonBankAccounts extends React.Component{
  // function deleteHandler(id) {
  //   dispatch({
  //     type: 'amazonbankaccounts/remove',
  //     payload: id,
  //   });
  // }
  //
  // function createHandler(values) {
  //   dispatch({
  //     type: 'amazonbankaccounts/applyAmzBanks',
  //     payload: Object.assign({}, values, { userId }),
  //   });
  // }
  //
  // function pageChangeHandler(page) {
  //   dispatch(routerRedux.push({
  //     pathname: '/amazon-bank-accounts',
  //     query: { page },
  //   }));
  // }

  render() {

    const columns = [
      {
        title: '店铺名',
        dataIndex: 'legalEntity.entityName',
        key: 'legalEntity.entityName',
      },
      {
        title: '银行账号',
        dataIndex: 'accountNumber',
        key: 'accountNumber',
        initialValue: '--',
      },
      {
        title: '路由号（9位）',
        dataIndex: 'routingNumber',
        key: 'routingNumber',
        initialValue: '--',
      },
      {
        title: '状态',
        dataIndex: 'status',
        key: 'status',
        render:(text,record,index)=>{
          var show = "N/A";
          if(text === 0){
            show = '审核中';
          } else if(text===1){
            show = <Icon type="check" className={styles.check}></Icon>;
          }
          return (
            <span>{show}</span>
          )
        }
      }

    ];
    const { dataSource,loading } = this.props;
    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    return (
      <div className={styles.normal}>
        <div>
          <PageTitle title="亚马逊收款账户">
            <Link to={"/home?userId="+userInfo.id}>
              <Button
                type="default" shape="circle" icon="rollback" size="default"
              ></Button>
            </Link>
            <Button style={{marginLeft:"32px"}}
              onClick={() => {
                this.props.clickNewAccountButton();
              }}
              type="primary" shape="circle" icon="plus" size="default" />
          </PageTitle>
          <div className={styles.authFormWarp}>
            <Table
              columns={columns}
              size="small"
              dataSource={dataSource}
              loading={loading}
              rowKey={record => record.id}
              pagination={true}
            />
          </div>
          {/*<Pagination*/}
          {/*className="ant-table-pagination"*/}
          {/*total={total}*/}
          {/*current={current}*/}
          {/*pageSize={PAGE_SIZE}*/}
          {/*onChange={pageChangeHandler}*/}
          {/*/>*/}
        </div>
      </div>
    );

  }




}

function mapStateToProps(state) {
  const { amazonBankAccounts, total, userId } = state.amazonBankAccounts;
  return {
    loading: state.loading.models.amazonBankAccounts,
    dataSource: amazonBankAccounts,
    total,
    userId,
  };
}

export default connect(mapStateToProps)(AmazonBankAccounts);

