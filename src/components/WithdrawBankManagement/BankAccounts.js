import React from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import { Table, Icon, Button, Col } from 'antd';
import styles from './BankAccounts.css';
import PageTitle from '../Commons/PageTitle';
import BankAccountDeleteModal from './BankAccountDeleteModal';

function BankAccounts({ dispatch, list: dataSource, loading }) {
  function deleteHandler(id) {
    dispatch({
      type: 'banks/remove',
      payload: id,
    });
  }

  const columns = [
    {
      title: '账户名',
      dataIndex: 'accountHolderName',
      key: 'accountHolderName',
    },
    {
      title: '账户号',
      dataIndex: 'accountNumber',
      key: 'accountNumber',
    },
    {
      title: '所属银行',
      dataIndex: 'bankName',
      key: 'bankName',
    },
    {
      title: '开户分行',
      dataIndex: 'bankBranchName',
      key: 'bankBranchName',
    },
    // {
    //   title: '',
    //   key: 'operation',
    //   className: styles.operationWrap,
    //   render: (text, record) => (
    //     <span className={styles.operation}>
    //       <BankAccountDeleteModal record={record} onOk={deleteHandler}>
    //         <Button type="primary" shape="circle" icon="minus" size="small" />
    //       </BankAccountDeleteModal>
    //     </span>
    //   ),
    // },
  ];
  const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
  return (
    <div className={styles.normal}>
      <PageTitle title="提现银行卡信息">
        <Link to={"/home?userId="+userInfo.id}>
          <Button
            type="default" shape="circle" icon="rollback" size="default"
          ></Button>
        </Link>
        <Link to="/withdraw-bank-accounts/new" style={{marginLeft:"32px"}}>
          <Button type="primary" shape="circle" icon="plus" size="default" />
        </Link>
      </PageTitle>
      <div className={styles.tableWrap}>
        <Table
          columns={columns}
          dataSource={dataSource}
          loading={loading}
          rowKey={record => record.id}
          size="small"
        />
      </div>
      <div className="text-center">
        {/*<Link to={"/home?userId="+userInfo.id}><span className="h5"><Icon type="rollback"> </Icon>返回我的钱包</span></Link>*/}
      </div>
    </div>
  );
}

function mapStateToProps(state) {
  const { list } = state.banks;
  return {
    loading: state.loading.models.banks,
    list,
  };
}

export default connect(mapStateToProps)(BankAccounts);
