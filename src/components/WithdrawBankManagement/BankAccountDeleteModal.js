import React, { Component } from 'react';
import { Row, Col, Modal, Form, Input } from 'antd';
import styles from './BankAccountDeleteModal.css';

const FormItem = Form.Item;

class BankAccountDeleteModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  showModelHandler = (e) => {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }
    this.setState({
      visible: true,
    });
  };

  hideModelHandler = () => {
    this.setState({
      visible: false,
    });
  };

  okHandler = () => {
    const { onOk } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        onOk(values);
        this.hideModelHandler();
      }
    });
  };

  render() {
    const { children } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { name, accountNumber } = this.props.record;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };

    return (
      <span>
        <span onClick={this.showModelHandler}>
          {children}
        </span>
        <Modal
          title="提示"
          visible={this.state.visible}
          onOk={this.okHandler}
          onCancel={this.hideModelHandler}
        >
          <Row>
            <Col span={24} className={styles.message}>删除银行卡将关闭以下服务</Col>
            <Col span={24} className={styles.message}>{accountNumber} 提现服务</Col>
          </Row>
          <Form horizontal onSubmit={this.okHandler}>
            <FormItem
              {...formItemLayout}
              label="支付密码"
            >
              {
                getFieldDecorator('password', {
                  initialValue: name,
                })(<Input type="password"/>)
              }
            </FormItem>
          </Form>
        </Modal>
      </span>
    );
  }
}

export default Form.create()(BankAccountDeleteModal);
