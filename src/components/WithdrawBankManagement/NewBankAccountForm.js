import React from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router'
import { Form, Input, Select, Button, Icon,Row,Col } from 'antd';
import PageTitle from '../Commons/PageTitle';
import styles from './NewBankAccountForm.css';
// import CountDownButton from '../RealNameAuthentication/CountDownButton';
import CountDownButton from '../Commons/CountDownButton'
const FormItem = Form.Item;
const Option = Select.Option;

class NewBankAccountForm extends React.Component {
  state={
    error: '',
    isGetCode: false,
  }
  onSubmit() {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const formData = this.props.form.getFieldsValue();
        console.log(formData)
        this.props.onSubmit(formData,(error)=>{
          this.setState({
            error:error.message
          })
        });
      }
    });
  }

  fetchVerificationCode(e,cb) {
    const { dispatch } = this.props;
    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    dispatch({
      type: 'banks/fetchVerificationCode',
      payload: { userId: userInfo.id },
    });
    cb();
  }

  // 处理倒计时结束状态重置
  handleComplete = (e) => {
    this.setState({
      isGetCode: false
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };
    let defaultUserLegalEntity = {};
    // console.info(this.props.userLegalEntities.length)
    if (this.props.userLegalEntities.length > 0) {
      defaultUserLegalEntity = this.props.userLegalEntities[0];
    }
    // const buttonText = 60;
    // let buttonCode = null;
    // if (this.state.isGetCode) {
    //   buttonCode = <CountDownButton size="default" value={buttonText} onComplete={this.handleComplete.bind(this)}></CountDownButton>
    // } else {
    //   buttonCode = <Button size="default" onClick={this.fetchVerificationCode.bind(this)}>获取手机验证码</Button>
    // }

    return (
      <div>
        <PageTitle title="添加银行卡" >
          <Link to={"/withdraw-bank-accounts"}>
            <Button
              type="default" shape="circle" icon="rollback" size="default"
            ></Button>
          </Link>
        </PageTitle>
        <Form className={styles.form}>
          <FormItem
            {...formItemLayout}
            label="国家"
          >
            {
              getFieldDecorator('country', {
                initialValue: 'CN',
                rules: [
                  { required: true, message: '请填写正确的提现国家' },
                ],
              })(
                <Select disabled>
                  <Option value="CN">中国</Option>

                </Select>)
            }

          </FormItem>

          <FormItem
            {...formItemLayout}
            label="币种"
          >
            {
              getFieldDecorator('currency', {
                initialValue: '1',
                rules: [
                  { required: true, message: '请填写正确的提现货币' },
                ],
              })(<Select disabled>
                <Option value="1">人民币</Option>

              </Select>)
            }

          </FormItem>

          <FormItem
            {...formItemLayout}
            label="开户名"
          >
            {
              getFieldDecorator('legalEntityId', {
                initialValue: '' + defaultUserLegalEntity.id,
                rules: [
                  { required: true, message: '请填写正确的银行账户' },
                ],
              })(<Select disabled>
                <Option value={'' + defaultUserLegalEntity.id}>{defaultUserLegalEntity.entityName}</Option>

              </Select>)
            }

          </FormItem>

          <FormItem
            {...formItemLayout}
            label="银行账户"
          >
            {
              getFieldDecorator('accountNumber', {
                initialValue: '',
                rules: [
                  { required: true, message: '请填写正确的银行账户号' },
                ],
              })(<Input />)
            }

          </FormItem>

          <FormItem
            {...formItemLayout}
            label="所属银行"
          >
            {
              getFieldDecorator('bankName', {
                initialValue: '',
                rules: [
                  { required: true, message: '请填写正确的开户银行' },
                ],
              })(<Input placeholder="招商银行" />)
            }

          </FormItem>

          <FormItem
            {...formItemLayout}
            label="开户分行"
          >
            {
              getFieldDecorator('bankBranchName', {
                initialValue: '',
                rules: [
                  { required: true, message: '请填写正确的开户银行分行' },
                ],
              })(<Input placeholder="北京中关村分行" />)
            }

          </FormItem>

          <FormItem
            {...formItemLayout}
            label="验证码"
            help={this.state.error}
            extra="验证码将发送到您实名注册时的手机号"
          >

            <Row gutter={16}>

              <Col span={12}>
                {
                  getFieldDecorator('verificationCode', {
                    initialValue: '',
                    rules: [
                      { required: true, message: '请填写正确的验证码' },
                    ],
                  })(<div>
                    <Input placeholder="8位手机验证码" />

                    {/*{buttonCode}*/}
                    {/*<Button onClick={this.fetchVerificationCode.bind(this)}>获取验证码</Button>*/}
                  </div>)
                }
              </Col>
              <Col span={12}>
                <CountDownButton onClick={this.fetchVerificationCode.bind(this)} >获取验证码</CountDownButton>
              </Col>
            </Row>


          </FormItem>

          <FormItem
            wrapperCol={{ offset: 6, span: 14 }}
          >
            <Button onClick={this.onSubmit.bind(this)}>提交</Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(Form.create()(NewBankAccountForm));
