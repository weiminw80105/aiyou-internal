import React from "react";
import { Col, Form, Input, Modal, Row } from "antd";
import CountDownButton from "../../components/Commons/CountDownButton";
import md5 from "md5";

const FormItem = Form.Item;
class RetrievePassword extends React.Component {

  onSubmit() {
    const { onSubmit } = this.props;
    this.props.form.validateFieldsAndScroll((err, vaues) => {
      if (!err) {
        const formData = this.props.form.getFieldsValue();
        onSubmit({
          verificationCode: formData.verificationCode,
          password: md5(formData.password),
        });
      }
    });
  }

  onFetchVerificationCode(e, cb) {
    const { onFetch } = this.props;
    console.log(onFetch)
    onFetch();
    cb();
  }

  checkPass(rule, value, callback) {
    const { validateFields } = this.props.form;
    if (value.length < 8) {
      callback("密码长度小于8位");
    }
    if (value) {
      validateFields(['repassword'], { force: true });
    }
    callback();
  }

  checkPass2(rule, value, callback) {
    const { getFieldValue } = this.props.form;
    if (value && value !== getFieldValue('password')) {
      callback('两次输入密码不一致!');
    } else {
      callback();
    }
  }


  render() {
    const { visible, } = this.props;
    const { getFieldDecorator } = this.props.form
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      }
    };
    return (
      <Modal
        visible={visible} title="修改密码"
        closable={false}
        onCancel={this.props.onCancel}
        onOk={this.onSubmit.bind(this)}

      >
        <Form layout="horizontal">
          <FormItem
            {...formItemLayout}
            label="验证码"
            extra="验证码将发送到您实名注册时的手机号"
          >
            <Row gutter={16}>
              <Col span={12}>
                {getFieldDecorator("verificationCode", {
                  rules: [{
                    required: true, message: "请输入验证码"
                  }]
                })(
                  <Input placeholder="请输入8位验证码" />
                )}

              </Col>
              <Col span={12}>
                <CountDownButton onClick={this.onFetchVerificationCode.bind(this)}>获取验证码</CountDownButton>
              </Col>
            </Row>

          </FormItem>
          <FormItem
            {...formItemLayout}
            label="支付密码"
            extra="支付密码应为不少于8位的数字、字母以及符号所组成"
          >
            {getFieldDecorator("password", {
              rules: [{
                required: true, message: "请输入新的支付密码"

              }, { validator: this.checkPass.bind(this) },]
            })(
              <Input type="password" />
            )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="确认密码"
          >
            {getFieldDecorator("repassword", {
              rules: [{
                required: true, message: "请再次确认支付密码"
              }, { validator: this.checkPass2.bind(this) }]
            })(
              <Input type="password" />
            )}
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

export default Form.create()(RetrievePassword)
