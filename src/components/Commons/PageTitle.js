import React from 'react';
import styles from './PageTitle.css';

function PageTitle({ title, children }) {
  return (
    <div className={styles.normal}>
      <h1 className={styles.title}>{title}</h1>
      <div className={styles.titleBtn}>
        {children}
      </div>
    </div>
  );
}

export default PageTitle;
