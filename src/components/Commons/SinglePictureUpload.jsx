import React from 'react';
import { Icon, message, Upload } from 'antd';
import styles from './SinglePictureUpload.css';

export default class SinglePictureUpload extends React.Component {
  state = {
    value: this.props.value,
  };

  componentWillReceiveProps(nextProps) {
    if ('value' in nextProps) {
      const value = nextProps.value;
      this.setState({
        value,
      });
    }
  }

  handleChange(info) {
    console.debug(info)
    if (info.file.status === 'done') {
      this.triggerChange(`https://id.transfer-express.net/${info.file.response.key}`);
    }
  }

  triggerChange(changedValue) {
    // Should provide an event to pass value to Form.
    // console.debug(changedValue);
    const onChange = this.props.onChange;
    if (onChange) {
      onChange(changedValue);
    }
  }

  beforeUpload(file) {
    const isJPG = (file.type === 'image/jpeg' || file.type === 'image/jpg' || file.type === 'image/png');
    if (!isJPG) {
      message.config({ top: 90 })
      message.error('上传图片格式不正确');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.config({ top: 90 })
      message.error('图像大小不能超过2M');
    }
    return isJPG && isLt2M;
  }

  render() {
    const config = {
      action: 'https://up.qbox.me/',
      data: { token: this.props.uptoken },
      multiple: false,
      showUploadList: false,
      headers: {
        "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzdXBwb3J0QHRyYW5zZmVyLWV4cHJlc3MubmV0Iiwicm9sZXMiOjEyNywiaWF0IjoxNDg2MDQwOTAxfQ.j75AQ-QFMiPib3p-ChTU7aXNcSoWBSWNDpoVyhIUIbU",
         "Accept": "application/json",
        // "Content-Type": "multipart/form-data"
      }
    }
    let imageUrl = this.state.value;
    return (
      <Upload {...config} className={styles.avatarUploader}
              beforeUpload={this.beforeUpload.bind(this)}
              onChange={this.handleChange.bind(this)}
      >
        {
          this.state.value ?
            <img src={imageUrl} role="presentation" alt="" className={styles.avatar}/> :
            <Icon type="plus" className={styles.avatarUploaderTrigger}/>
        }
      </Upload>
    );
  }
}
