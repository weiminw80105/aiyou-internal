import React from 'react';
import { Button } from 'antd';

export default class CountDownButton extends React.Component {


  constructor(props) {
    super(props);
    this.initValue = this.props.value || 60;
    this.state = { count: this.initValue, disabled: false };
    this.interval = 0;
    this.step = this.props.step || 1;
  }
  onClick(e) {
    // const rt = this.props.onClick && this.props.onClick(e);
    e.preventDefault();
    this.props.onClick &&this.props.onClick(e,()=>{
      this.setState({
            disabled: true,
          })
          this.start();
      })
    // if(rt){
    //   this.setState({
    //     disabled: true,
    //   })
    //   this.start();
    // }


  }

  stop() {
    clearInterval(this.interval);
  }

  start() {
    this.stop();
    this.interval = setInterval(() => {
      let count = this.state.count - this.step;
      if (count === 0) {
        this.setState({
          disabled: false,
        })
        this.stop();
      } else {
        this.setState({ count });
      }

    }, 1000);
  }

  componentWillUnmount() {
    this.stop();
  }

  render() {
    return(<Button {...this.props} disabled={this.state.disabled} onClick={this.onClick.bind(this)}> {this.state.disabled ? '('+this.state.count+'秒)':''} {this.props.children} </Button>)
  }
}
