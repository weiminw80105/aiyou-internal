import React from "react";
import md5 from 'md5'
import { Form, Input, Modal, Button } from "antd";
import { connect } from "dva";

const FormItem = Form.Item;
class WaitForAuthentication extends React.Component {
  state = {
    setPasswordVisible: true,
  }
  componentWillMount() {
    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
  }
  render() {
    return (
      <Modal
        title="实名认证中..."
        visible={this.state.setPasswordVisible}
        closable={false}
        footer={null}
      >
        请您耐心等待, 实名认证会在24小时内完成， 审核通过后方可使用亚马逊收款服务（ 我们会以短信的形式通知您， 请注意查看)
      </Modal>

    );
  }
}


function mapStateToProps(state) {
  return {
  }
}

export default connect(mapStateToProps)(Form.create()(WaitForAuthentication));


