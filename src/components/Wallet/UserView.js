import React from 'react';
import { Card, Col, Row } from 'antd';
import { Link } from 'dva/router';

export default class UserView extends React.Component {
  render() {
    return (
      <div>
        {/*<Row gutter={32} >*/}
          {/*<Col span={12}>*/}
            {/*<Card bordered={true}>*/}
              {/*<Row type="flex" align="middle">*/}
                {/*<Col span={18}>*/}
                  {/*<div>账户余额</div>*/}
                  {/*<span className="h4">${this.props.userInfo ? this.props.userInfo.balance : '0' }</span>*/}
                {/*</Col>*/}
                {/*<Col span={6}>*/}
                  {/*<h4><Link to="withdraw">提现</Link></h4>*/}
                {/*</Col>*/}
              {/*</Row>*/}
            {/*</Card>*/}
          {/*</Col>*/}
          {/*<Col span={12}>*/}
            {/*<Card bordered={true}>*/}
              {/*<Row>*/}
                {/*<Col span={12}>*/}
                  {/*<strong>亚马逊收款账户</strong></Col><Col span={12}><Link to="/amazon-bank-accounts">管理</Link>*/}
              {/*</Col>*/}
              {/*</Row>*/}

              {/*<Row>*/}
                {/*<Col span={12}>*/}
                  {/*<strong>提现银行卡</strong></Col><Col span={12}><Link to="/withdraw-bank-accounts">管理</Link>*/}
                {/*</Col>*/}
              {/*</Row>*/}

              {/*<Row>*/}
                {/*<Col span={12}>*/}
                  {/*<strong>安全中心</strong></Col><Col span={12}><Link to="/security">管理</Link>*/}
                {/*</Col>*/}
              {/*</Row>*/}
            {/*</Card>*/}
          {/*</Col>*/}
        {/*</Row>*/}
        <Card>
        <Row type='flex' align="middle" justify="center">
              <Col span={6}>

                <div>账户余额</div>
                <div className="h4">${this.props.userInfo ? this.props.userInfo.balance : '0' } <Link to="withdraw" style={{marginLeft:"32px"}}>提现</Link></div>
              </Col>
              <Col span={6}>

                <dl className="dl-horizontal">
                  <dt>亚马逊收款账户</dt><dd><Link to="/amazon-bank-accounts">管理</Link></dd>
                  <dt>提现银行卡</dt><dd><Link to="/withdraw-bank-accounts">管理</Link></dd>
                  <dt>安全中心</dt><dd><Link to="/security">管理</Link></dd>
                </dl>

              </Col>



        </Row>
      </Card>
      </div>
    );
  }
}

