import React from "react";
import md5 from 'md5'
import { Form, Input, Modal, Button, Row, Col } from "antd";
import { connect } from "dva";
import CountDownButton from '../Commons/CountDownButton'

const FormItem = Form.Item;
class NewPasswordForm extends React.Component {
  state = {
    setPasswordVisible:false,
  }
  componentWillMount(){
    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    console.info(userInfo);
    if(!userInfo.hasTransPassword){
        this.setState({
          setPasswordVisible:true,
        })
    }

  }
  onOk() {
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const formData = this.props.form.getFieldsValue();
        const { dispatch } = this.props;
        const userInfo = JSON.parse(sessionStorage.getItem('userInfo'))
        var _this = this;
        dispatch({
          type: 'wallet/setTransPassword',
          payload: { userId: userInfo.id,
            transactionPassword:md5(formData.transactionPassword),
            verificationCode:formData.verificationCode
          },
          onComplete(){
            _this.setState({
              setPasswordVisible:false,
            })
          }
        });
      }
    });
  }

  fetchVerificationCode(e,cb) {
    const { dispatch } = this.props;
    const userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    dispatch({
      type: 'users/fetchVerificationCode',
      payload: { userId: userInfo.id },
    });
    cb();
  }

  checkPass(rule, value, callback) {
    const {validateFields} = this.props.form;
    if(value.length <8){
      callback("密码长度小于8位");
    }
    if (value) {
      validateFields(['repassword'], {force: true});
    }
    callback();
  }

  checkPass2(rule, value, callback) {
    const {getFieldValue} = this.props.form;
    if (value && value !== getFieldValue('transactionPassword')) {
      callback('两次输入密码不一致!');
    } else {
      callback();
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };
    return (
      <Modal
        title="设置支付密码"
        visible={this.state.setPasswordVisible}
        closable={false}
        footer={<Button type="primary" onClick={this.onOk.bind(this)}>确认</Button>}
      >

        <Form layout="horizontal">
          <FormItem
            {...formItemLayout}
            label="提示"
          >
            <span className="text-warning">
              为确保您的资金安全，请设置您的支付密码
            </span>
          </FormItem>
          <FormItem
            {...formItemLayout}
            label="密码"
            extra="支付密码应为不少于8位的数字、字母以及符号所组成"
          >
            {
              getFieldDecorator('transactionPassword', {
                initialValue: '',
                rules: [
                  { required: true, message: '请填写支付密码' },
                  {validator: this.checkPass.bind(this)},
                ],
              })(
                <Input type="password" />
              )
            }
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="确认密码"
          >
            {
              getFieldDecorator('repassword', {
                initialValue: '',
                rules: [
                  { required: true, message: '请再次输入支付密码' },
                  {validator: this.checkPass2.bind(this)},
                ],
              })(
                <Input type="password" />
              )
            }
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="验证码"
            extra="验证码将发送到您实名注册时的手机号"
          >
            <Row gutter={16}>
              <Col span={12}>
                {
                  getFieldDecorator('verificationCode', {
                    initialValue: '',
                    rules: [
                      { required: true, message: '输入验证码' },
                    ],
                  })(
                    <Input placeholder="8位手机验证码" />
                  )
                }

              </Col>
              <Col span={12}>
                <CountDownButton onClick={this.fetchVerificationCode.bind(this)}>获取验证码</CountDownButton>
              </Col>
            </Row>
          </FormItem>


        </Form>
      </Modal>

    )
  }
}


function mapStateToProps(state) {
  return {

  }
}

export default connect(mapStateToProps)(Form.create()(NewPasswordForm));


