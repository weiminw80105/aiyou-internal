import React from 'react';
import { Table } from 'antd';
import styles from './Transaction.css';
import { currency } from '../../constants';


export default class UserTransactions extends React.Component {
  render() {
    const { dataSource, loading, total, current } = this.props;
    const columns = [
      {
        title: '日期',
        dataIndex: 'transactionTime',
        key: 'transactionTime'
      },
      {
        title: '交易类型/账号',
        dataIndex: 'accountNumber',
        key: 'accountNumber',
        render: (text, record, index) => {
          var transactionType;
          if (record.transactionType === 1) {
            transactionType = '入账';
          } else if (record.transactionType === 2) {
            transactionType = '提现';
          }
          return (
            <div>
              <div><strong>{transactionType}</strong></div>
              <div>{text}</div>
            </div>
          )
        }
      },
      {
        title: '金额',
        dataIndex: 'amount',
        key: 'amount',
        render: (text, record, index) => {
          return (
            <div><label>$</label> {text}</div>
          )

        }
      },
      {
        title: '汇率',
        dataIndex: 'exchangeRate',
        key: 'exchangeRate',
      },
      {
        title: '手续费/实际到账',
        dataIndex: 'actualAmount',
        key: 'actualAmount',
        render: (text, record, index) => {
          return (
            <div>
              <div><label>手续费: $</label> {record.fee}</div>
              <div><label>实际到账: {currency[record.currency - 1]}</label> {text}</div>
            </div>
          )
        }
      }, {
        title: '状态',
        dataIndex: 'status',
        key: 'status',
        render: (text, record, index) => {
          var statusText = '未知';
          if (text === 16) {
            statusText = '银行处理中'
          } else if (text === 18) {
            statusText = '银行处理完毕'
          } else if (text === 32) {
            statusText = '银行处理中'
          } else if (text === 34) {
            statusText = '银行处理完毕'
          }

          return (
            <span>{statusText}</span>
          )
        }
      },
    ];
    return (
      <div className={styles.normal}>
        <div>

          <Table
            size="small"
            columns={columns}
            dataSource={this.props.transactions}
            loading={this.props.loading}
            rowKey={record => record.transactionKey}
            pagination={true}
          />

        </div>
      </div>
    );
  }
}

